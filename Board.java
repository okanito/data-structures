/**
 * Board Class
 * The Board Class represents the table of the game and contains the following attributes:
 * - int N, M, the dimensions of the table
 * - int[][] tiles, is the 2D table of dimension NxM, that contains the ids of the tiles,
 *   with values ranging from 1 to NxM
 * - Snake[] snakes, a table containing objects of type Snake
 * - Ladder[] ladders, a table containing objects of type Ladder
 * - Apple[] apples, a table containing objects of type Apple
 *
 * The class contains three constructors, Setter and Getter functions as well as two functions
 * createBoard and createElementBoard that are described in detail later on
 */
public class Board {

    private int N, M;
    private int[][] tiles;
    Snake[] snakes;
    Ladder[] ladders;
    Apple[] apples;


    /**
     * Empty Constructor
     */
    public Board() {
    }

    /**
     * Typical constructor for initializing the Board attributes
     * @param n
     * @param m
     * @param numSnakes
     * @param numLadders
     * @param numApples
     */
    public Board(int n, int m, int numSnakes, int numLadders, int numApples) {
        N = n;
        M = m;
        tiles = new int[N][M];
        snakes = new Snake[numSnakes];
        ladders = new Ladder[numLadders];
        apples = new Apple[numApples];
    }

    /**
     * Constructor with Board object as an argument
     * @param a, the given board object
     */
    public Board(Board a) {
        N = a.N;
        M = a.M;
        tiles = a.tiles;
        snakes = a.snakes;
        ladders = a.ladders;
        apples = a.apples;
    }

    // Setter Methods

    void setN(int n) {
        N = n;
    }

    void setM(int m) {
        M = m;
    }

    void setTile(int[][] Ti) {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                tiles[i][j] = Ti[i][j];
            }
        }
    }

    void setSnake(Snake[] Sn) {
        System.arraycopy(snakes, 0, Sn, 0, snakes.length);
    }

    void setLadder(Ladder[] La) {
        System.arraycopy(ladders, 0, La, 0, ladders.length);
    }

    void setApple(Apple[] Ap) {
        System.arraycopy(apples, 0, Ap, 0, apples.length);
    }

    // Getter Methods

    int getN() {
        return N;
    }

    int getM() {
        return M;
    }

    int[][] getTiles() {
        return tiles;
    }

    Snake[] getSnakes() {
        return snakes;
    }

    Ladder[] getLadders() {
        return ladders;
    }

    Apple[] getApples() {
        return apples;
    }

    /**
     * createBoard function
     * Randomly creates a board for the current game by:
     * - initializing the tiles board with the ids of the tiles
     * - randomly initializing the objects of the "snakes", "ladders" and "apples" boards,
     *   in the playing board.
     */
    void createBoard() {

        //Initialization of the "tiles" Board with ids of the tiles
        int i, j;
        for (j = 0; j < N; j++) {
            for (i = 0; i < M; i++) {
                if ((N + 1) % 2 == 0) {
                    //check if the board has an even number of columns
                    if (j % 2 == 0) {
                        //if the number of the column is odd or zero
                        tiles[j][i] = M * N - (j * M + M - i - 1);
                    } else {
                        tiles[j][i] = M * N + (-j * M - i);
                    }
                } else {
                    //if the board has an odd number of columns
                    if (j % 2 == 0 || j == 0) {
                        //if the number of columns is even or zero
                        tiles[j][i] = M * N + (-j * M - i);
                    } else {
                        tiles[j][i] = M * N - (j * M + M - i - 1);
                    }
                }
            }
        }
        for (j = 0; j < N; j++) {
            for (i = 0; i < M; i++) {
                System.out.print(tiles[j][i] + " ");
            }
            System.out.println();
        }

        //Initialization of Snakes Board
        for (i = 0; i < snakes.length; i++) {
            int s;
            snakes[i] = new Snake();
            snakes[i].setSnakeId(i + 1);
            s = 1 + (int) (Math.random() * (N * M - 1) + M);
            snakes[i].setHeadId(s);
            snakes[i].setTailId(1 + (int) (Math.random() * (s - M) + 1));
        }

        //Initialization of Ladders Board
        for (i = 0; i < ladders.length; i++) {
            int l;
            ladders[i] = new Ladder();
            ladders[i].setLadderId(i + 1);
            l = 1 + (int) (Math.random() * (N * M - 1) + M);
            ladders[i].setUpStepId(l);
            ladders[i].setDownStepId(1 + (int) (Math.random() * (l - M) + 1));
            ladders[i].setBroken(false);
        }

        //Initialization of Apples Board
        for (i = 0; i < apples.length; i++){
            int n = 0, k = 0;
            int color = 0;
            apples[i] = new Apple();
            apples[i].setAppleId(i + 1);
            //We avoid placing  an apple on the same tile as a snakes head
            do {
                n = 1 + (int) (Math.random() * (N * M - 1) + 1);
                for (j = 0; j < snakes.length; j++) {
                    k = 0;
                    if (snakes[j].getHeadId() == n) {
                        k = 1;
                    }
                }
            } while (k == 1);

            apples[i].setAppleTileId(n);
            color = (int) Math.round(Math.random());
            //num refers to color and it is either 1 for red or 0 for black
            if (color == 1) {
                apples[i].setColor("red");
                apples[i].setPoints(5);
            } else if (color == 0) {
                apples[i].setColor("black");
                apples[i].setPoints(-5);
            }
        }
    }

    /**
     * createElementBoard function
     * Creates and prints three boards, one for every entity of the game
     * Snakes, Ladders and Apples with the same dimensions as the tiles board.
     * Each board cell contains the string representation of the object of this particular cell.
     *  - Snake Head: SH, Snake Tail: ST
     *  - Bottom of a Ladder: LD, Ladder Top: LU
     *  - Red Apple: AR, Black Apple: AB
     */
    void createElementBoard() {

        //Creates and prints the boards for the 2 elements apples, snakes and ladders
        int i;
        int j;
        int k;
        String[][] elementBoardSnakes = new String[N][M];
        String[][] elementBoardLadderId = new String[N][M];
        String[][] elementBoardApples = new String[N][M];

        //elementBoardSnakes
        for(j=0;j<N; j++) {
            for (i = 0; i < M; i++) {
                for (k = 0; k < snakes.length; k++) {
                    if (snakes[k].getHeadId() == tiles[j][i]) {
                        elementBoardSnakes[j][i] = "SH" + snakes[k].getSnakeId();
                    } else if (snakes[k].getTailId() == tiles[j][i]) {
                        elementBoardSnakes[j][i] = "ST" + snakes[k].getSnakeId();
                    }
                }
                if (elementBoardSnakes[j][i] == null) {
                    elementBoardSnakes[j][i] = "_";
                }
            }
        }

        //Prints out the the entire elementBoardSnakes board
        System.out.println("\n\n-----!ElementBoardSnakes!-----");
        for (j = 0; j < N; j++) {
            for (i = 0; i < M; i++) {
                System.out.print(elementBoardSnakes[j][i] + " ");
            }
            System.out.println();
        }
        System.out.println("\n\n");


        //elementBoardLadderId
        for (j = 0; j < N; j++) {
            for (i = 0; i < M; i++) {
                //elementBoardSnakes[i][j]= new String;
                for (k = 0; k < ladders.length; k++) {
                    if (ladders[k].getUpStepId() == tiles[j][i]) {
                        elementBoardLadderId[j][i] = "LU" + ladders[k].getLadderId();
                    } else if (ladders[k].getDownStepId() == tiles[j][i]) {
                        elementBoardLadderId[j][i] = "LD" + ladders[k].getLadderId();
                    }
                }
                if (elementBoardLadderId[j][i] == null) {
                    elementBoardLadderId[j][i] = "_";
                }
            }
        }

        //Prints out the entire elementBoardLadderId board
        System.out.println("-----!ElementBoardLadderId!-----");
        for (j = 0; j < N; j++) {
            for (i = 0; i < M; i++) {
                System.out.print(elementBoardLadderId[j][i] + " ");
            }
            System.out.println();
        }

        System.out.println("\n\n");

        for (i = 0; i < N; i++) {
            for (j = 0; j < M; j++) {
                for (k = 0; k < apples.length; k++) {
                    if ((apples[k].getAppleTileId() == tiles[i][j]) && (apples[k].getColor() == "red"))
                        elementBoardApples[i][j] = "AR" + apples[k].getAppleId();
                    else if ((apples[k].getAppleTileId() == tiles[i][j]) && (apples[k].getColor() == "black"))
                        elementBoardApples[i][j] = "AB" + apples[k].getAppleId();
                }
                if (elementBoardApples[i][j] == null) {
                    elementBoardApples[i][j] = "_";
                }
            }
        }

        System.out.println("-----!ElementBoardApple!-----");
        for (i = 0; i < N; i++) {
            for (j = 0; j < M; j++) {
                System.out.print(elementBoardApples[i][j] + " ");
            }
            System.out.println();
        }
    }
}
