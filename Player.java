public class Player {

    private int playerId;
    private String name;
    private int score;
    private Board board;

    // Empty Constructor
    public Player() {
        
    }

    public Player(int Id, String onoma, int sc, Board b) {
        playerId = Id;
        name = onoma;
        score = sc;
        board = new Board(b);

    }

    void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    void setName(String name) {
        this.name = name;
    }

    void setScore(int score) {
        this.score = score;
    }

    void setBoard(Board b) {
        board = new Board(b);
    }

    int getPlayerId() {
        return playerId;
    }

    String getName() {
        return name;
    }

    int getScore() {
        return score;
    }

    Board getBoard() {
        return board;
    }

    int[] move(int id, int die) {
        int j = 1;
        int newTile = (id + die);
        int laddersSum = 0;
        int snakesSum = 0;
        int redApplesSum = 0;
        int blackApplesSum = 0;
        do {
            j = 0;
            for (int i = 0; i < board.apples.length; i++) {
                if (newTile == board.apples[i].getAppleTileId() && board.apples[i].getColor() == "red" && board.apples[i].getPoints() != 0) {
                    score += board.apples[i].getPoints();
                    board.apples[i].setPoints(0);
                    redApplesSum++;
                    System.out.println(name + " just ate a red apple\n");
                    j = 1;
                } else if (newTile == board.apples[i].getAppleTileId() && board.apples[i].getColor() == "black" && board.apples[i].getPoints() != 0) {
                    score += board.apples[i].getPoints();
                    board.apples[i].setPoints(0);
                    blackApplesSum++;
                    System.out.println(name + " just ate a black apple.\n");
                    j = 1;
                }
            }

            for (int i = 0; i < board.ladders.length; i++) {
                if (newTile == board.ladders[i].getDownStepId() && board.ladders[i].getBroken() == false) {
                    newTile = board.ladders[i].getUpStepId();
                    laddersSum++;
                    board.ladders[i].setBroken(true);
                    System.out.println(name + " just climbed up a ladder and the ladder broke");
                    j = 1;
                }
            }

            for (int i = 0; i < board.snakes.length; i++) {
                if (newTile == board.snakes[i].getHeadId()) {
                    newTile = board.snakes[i].getTailId();
                    snakesSum++;
                    System.out.println(name + " just got bitten by a snake");
                    j = 1;
                }
            }
        } while (j == 1);
        int[] array = {newTile, snakesSum, laddersSum, redApplesSum, blackApplesSum};
        return array;
    }
}